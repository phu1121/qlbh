package com.qlbh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SellManagermentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SellManagermentApplication.class, args);
    }

}
